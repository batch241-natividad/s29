db.users.insertMany([
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 21, 
		contact: {
			phone: "87654321",
			email: "janedoe@gmail.com"
		}, 
		courses: [ "CSS", "Javascript", "Python" ], 
		department: "HR"
	},
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76, 
		contact: {
			phone: "87654321",
			email: "stephenhawking@gmail.com"
		}, 
		courses: [ "Python", "React", "PHP" ], 
		department: "HR"
	},
    {
        firstName: "Neil",
        lastName: "Armstrong",
        age: 82,
        contact: {
            phone: "87654321",
            email: "neilarmstrong@gmail.com"
        },
        courses: [ "React", "Laravel", "Sass" ],
        department: "HR"
    },
	{
        firstName: "Bill",
        lastName: "Gates",
        age: 65,
        contact: {
            phone: "12345678",
            email: "bill@gmail.com"
        },
        courses: ["PHP", "Laravel", "HTML"],
        department: "Operations",
        status: "active"
    }
])

// COMPARISON QUERY OPERATORS

//  $gt / $gte operator

/*
-Allows us to have documents that have field number values greater that or equal to  a specific value.

-Syntax:
	db.collectionName.find({field: {$gt: value}})
	db.collectionName.find({field: {$gte: value}})

*/

db.users.find({age:{$gt:65});
db.users.find({age:{$gte:65});

//$lt / $lte operators
/*
-Allows us to find documents that have field number less than or equal to a specified value 

-Syntax:
	db.collectionName.find({field:{$lt:value}})
	db.collectionName.find({field:{$lte:value}})


*/

db.users.find({age:{$lte:76});
db.users.find({age:{$lt:76}});

//$ne operator
/*
-Allows us to find documents that have a specified value that are not equal to a specified value.

- Syntax:
	db.collectionName.find({field:{$ne:value}})
*/

db.users.find({age:{$ne:82}})


//LOGICAL QUERY OPERATORS


//$or
/*
	
- allows us to find documents that match a single criteria from multiple search criteria provided.
- Syntax:
	db.collectionName.find({$or:[{"fieldA: valueA"},  {"fieldB: valueB"}]})

*/

db.users.find({$or: [{firstName: "Neil"}, {firstName: "Bill"}]})
db.users.find({$or: [{firstName: "Neil"}, {firstName: "Stephen"},{firstName: "Bill"},{firstName: "Jane"}]})

db.users.find({$or: [{firstName:"Neil"}, {age:{$gt:30}}]});

// $and operator

/*
-Allows us to find documents matching multiple criteria in a single field.
	-Syntax:
		db.collectionName.find({$and:[{"fieldA: valueA",{"fieldB: valueB"}}]})

*/

db.users.find({$and:[{age:{$ne:82}}, {age:{$ne:76}}]})
db.users.find({$and:[{age:{$lt:65}},{age:{$gt:65}}]})

//Field projection
/*
-When we retrieve documents, MongoDB usually returns the whole document.
- There are times when we only need specific fields.
-For those cases, we can include or exclude fields from the response.
*/


//Inclusion
/*
-Allows us to include / add specific fields only when retrieving documents.
-We write 1 to include fields
- Syntax:
	db.users.find({criteria}, {field:1})
*/

db.users.find(
{
	firstName: "Jane"

},
{
	firstName:1,
	lastName:1,
	"contact.phone": 1
}
)

//EXCLUSION
/*
-Allows us to exclude / remove specific fields when displaying documents
- The value provided is "0" to denote that the field is being excluded.
-Syntax:
	db.users.find({criteria}, {field:0})

*/

db.users.find(
	{
		firstName:"Jane"
	}, 


{

	department:0,
	contact:0
}
)

//Supressing the ID field
/*
-Allows us to exclude the "_id" field when retrieving documents.
-When using field projection, field inclusion and exclusion may not be used at the same time.
-The "_id" is exempted to this rule.
- Syntax:
	db.users.find({criteria},{_id:0})

*/

db.users.find(
{
	firstName: "Jane"

},
{
	firstName:1,
	lastName:1,
	contact:1,
	_id:0
})

//Evaluation of query operators

//$regex operator

/*
- Allows us to find documents that match a specific string pattern using regular expressions.
- Syntax:
	db.users.find({field: $regex: 'pattern',$options:'$optionsValue'})
*/


//case sensitive query
db.users.find({firstName: {$regex: 'N'}})


//Case Insensitive query
db.users.find({firstName: {$regex: 'N', $options: '$i'}})